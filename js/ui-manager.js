//#region Add Listeners

//obter mensagens
let nameInput = document.getElementById("name-input");
let messageInput = document.getElementById("message-input");
let messagesSection = document.getElementById("messages");

document.querySelector("#snd-msg-btn").addEventListener("click", e => {
    if (nameInput.value === "" || messageInput.value === "") {
        alert("Preencha os campos com algum valor");
        return;
    }
    
    sendMessage(nameInput.value, messageInput.value).then(e => {
        getMessagesFromServer();
    });
});

document.querySelector("#get-msg-btn").addEventListener("click", e => {
    //obter mensagens do servidor
    getMessagesFromServer(url);
});

let filterInput = document.querySelector("#msg-input-find");
document.querySelector("#get-msg-by-txt").addEventListener("click", e => {
    showMessages(filterInput.value);
});

//#endregion



//#region Update dom manip

let messageBoxes = [];

function getMessagesFromServer() {
    deleteMessages();
    getMessages().then(objects => {
        for (let i = 0; i < objects.length; i++) {
            messageBoxes.push(new MessageBox(objects[i]["id"], objects[i]["name"], objects[i]["message"]));
        }
    });
}

function deleteMessages() {
    messageBoxes = [];
    messagesSection.innerHTML = "";
}

function showMessages(filter) {
    for (let i in messageBoxes) {
        messageBoxes[i].show(parseInt(messageBoxes[i].id) == parseInt(filter) || filter == "");
    }
}

//#endregion