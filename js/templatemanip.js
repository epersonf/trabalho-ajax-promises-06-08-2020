const template = document.querySelector(".template");
const insertSection = document.querySelector("#messages");

class MessageBox {

    constructor(id, name="Default", message="Lorem ipsum...") {
        this.node = template.cloneNode(true);
        insertSection.appendChild(this.node);

        this.id = id;

        this.nameField = this.node.querySelector(".user-name");
        this.nameEditInput = this.node.querySelector(".user-name-edit");

        this.messageField = this.node.querySelector(".message-field");
        this.messageInputEdit = this.node.querySelector(".message-field-edit");

        //#region DeleteButton

        this.deleteButton = this.node.querySelector(".delete-button");
        
        this.deleteButton.addEventListener("click", e => {
            
            for (let i in messageBoxes) {
                if (messageBoxes[i] == this) {
                    messageBoxes.splice(i, 1);
                    break;
                }
            }

            this.deleteFromDB();
        });
        //#endregion

        this.editMenu = this.node.querySelector(".edit-menu");
        this.editButton = this.node.querySelector(".edit-button");

        //#region ConfirmEditButton
        this.confirmEditButton = this.node.querySelector(".confirm-edit-button");

        this.confirmEditButton.addEventListener("click", () => {
            this.confirmEdit();
        });
        //#endregion

        this.nameField.innerHTML = name;
        this.messageField.innerHTML = message;

        this.editButton.addEventListener("click", () => {
            this.makeEditable(!this.beingEdited);
        });

        this.beingEdited = false;
        this.makeEditable(this.beingEdited);
        this.isVisible = true;
    }

    //#region Non-static

    //#region Edit methods
    makeEditable(v) {
        this.beingEdited = v;
        MessageBox.setNodeVisibility(this.editMenu, v);
        if (v) {
            this.nameEditInput.value = this.nameField.innerHTML;
            this.messageInputEdit.value = this.messageField.innerHTML;
        }
    }

    confirmEdit() {
        if (!this.beingEdited) return;
        this.nameField.innerHTML = this.nameEditInput.value;
        this.messageField.innerHTML = this.messageInputEdit.value;
        editMessageBox(this.id, this.nameEditInput.value, this.messageInputEdit.value);
        this.makeEditable(false);
    }
    //#endregion


    show(v) {
        this.isVisible = v;
        MessageBox.setNodeVisibility(this.node, v);
    }

    removeNode() {
        this.node.remove();
    }

    deleteFromDB() {
        this.removeNode();
        deleteMessageBox(this.id);
    }

    //#endregion

    //#region Static methods

    static getNodeVisibility(node) {
        return node.style.display == "none" ? true : false;
    }

    static setNodeVisibility(node, v) {
        if (v) node.style.display = "";
        else node.style.display = "none";
    }

    //#endregion
}