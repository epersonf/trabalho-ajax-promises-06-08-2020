var url = "https://treinamentoajax.herokuapp.com/messages/";


async function getMessages() {
    let toReturn;
    try {
        await fetch(url)
        .then(response => response.json())
        .then(response => toReturn = response);
    } catch (e) {
        console.warn(e);
    }
    return toReturn;
}

async function deleteMessageBox(id) {
    await fetch(url + id, {
        method:"DELETE"
    });    
}

async function sendMessage(name="defaultName", message="defaultMsg") {
    try {
        let fetchBody = generateFetchBody(name, message);

        let fetchConfig = generateFetchConfig("POST", fetchBody);

        await fetch(url, fetchConfig);
    } catch(e) {
        console.warn(e);
    }
}

async function editMessageBox(id, newAuthor, newMessage) {
    try {
        let fetchBody = generateFetchBody(newAuthor, newMessage);

        let fetchConfig = generateFetchConfig("PUT", fetchBody);


        await fetch(url + id, fetchConfig);
        getMessagesFromServer();
    } catch(e) {
        console.warn(e);
    }
}

function generateFetchBody(author, message) {
    return {
        "message": {
            "name": author,
            "message": message
        }
    };
}

function generateFetchConfig(method, fetchBody) {
    return {
        method: method,
        headers: {"Content-Type": "application/JSON"},
        body: JSON.stringify(fetchBody)
    };
}

