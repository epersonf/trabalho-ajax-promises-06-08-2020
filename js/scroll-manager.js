const maxHeaderYPos = 500;

let headerElement = document.querySelector("header");
let scrollUpBtn = document.querySelector("#up-button");


scrollUpBtn.addEventListener("click", e => {
    $("html, body").animate({scrollTop: 0 }, 1000);
});


window.onload = window.onscroll = updateScroll;

const timelineH2 = $('#timeline-h2');

function updateScroll() {
    if (window.scrollY < timelineH2.offset().top) {
        headerElement.className = "header-active";
        scrollUpBtn.style.display = "none";
    } else {
        headerElement.className = "";
        scrollUpBtn.style.display = "";
    }
}
